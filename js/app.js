// 1. Creemos las variables que vamos a necesitar en principio
// Entonces: en esta primera parte los botones y el formulario como tal
const btnEnviar = document.querySelector('#enviar');
const btnReset = document.querySelector('#resetBtn');
const formulario = document.querySelector('#enviar-mail');

// 2. Ahora las variables que representan los campos del formulario
const email = document.querySelector('#email');
const asunto = document.querySelector('#asunto');
const mensaje = document.querySelector('#mensaje');

// 11.1 --> Una expresión regular es una buena práctica para validar un email, por ahora se recomienda
// el uso de un generador de esta expresión: http://emailregex.com/
const validarEmail = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

// 3. Creamos la función que reune los addEventListener(es una función opcional, o más bien, una manera de trabajar)
eventListener();
function eventListener() {
  // 5. activamos la función iniciarApp, esto con el fin de que sea lo primero que cargue
  // Cuando la app arranca o carga dispara la función iniciarApp
  document.addEventListener('DOMContentLoaded', iniciarApp);
  // 7. Campos del formulario, el evento blur se dispara cuando un elemento pierde su foco.
  email.addEventListener('blur', validarFormulario);
  asunto.addEventListener('blur', validarFormulario);
  mensaje.addEventListener('blur', validarFormulario);

  // 14. Importante, darle acción a los botones enviar y resetear
  formulario.addEventListener('submit', enviarEmail);

  btnReset.addEventListener('click', resetFormulario);
}

// 4. Vamos a crear el efecto que deshabilita el botón de enviar mientras no estén los campos llenos y validados
function iniciarApp() {
  btnEnviar.disabled = true;
  // la clase cursor-not-allowed es cambiar el cursor. 
  btnEnviar.classList.add('cursor-not-allowed')
}

//6. Validemos la información que llega al formulario
function validarFormulario(e) {
  // 8. Lo primero que haremos es validar que ningún campo esté vacío, 'value' es el atributo que me valida lo que hay dentro de un campo.
  if (e.target.value.length > 0) {
    
    //10. Luego de que garantizamos que el campo no está vacío, y de haber validado este error
    // Procedemos a validar campo por campo

// Hay un paso importante a tener en cuenta es que no queremos llenar el formulario de errores
// Entonces, vamos a hacerle una validación al error.
// Creamos na variable que reconozca el elemento que creamos para mostrar el error(ver el comentario del punto 9)        
  const error = document.querySelector('p.msjError');
    if(error) {
      error.remove();
    }
    e.target.classList.remove('campoInvalido');
    e.target.classList.add('campoValido');
} else {
  e.target.classList.add('campoInvalido');    
  e.target.classList.remove('campoValido');    
  mostrarError('Todos los campos son obligatorios');
  }
  // 11. Vamos a validar el email y en el punto 11.1 --> vamos a crear una expresión regular que nos valide las condiciones del email.
  if (e.target.type === 'email') {
    // Usaremos la funcion test para validar el value del campo donde tenemos el email
    if (validarEmail.test( e.target.value )) {
      // seleccionamos el párrafo que creamos para el error
      const error = document.querySelector('p.msjError');
      if(error) {
        error.remove();
      }
      e.target.classList.remove('campoInvalido');
      e.target.classList.add('campoValido');
    } else {
      e.target.classList.remove('campoValido');
      e.target.classList.add('campoInvalido');
      mostrarError('Email no válido');
    }    
  }
  // En este if lo que hacemos es validar que todos los campos cumplan los requisitos para habilitar de nuevo el botón enviar
  if( validarEmail.test( email.value ) && asunto.value !== '' && mensaje.value !== '') {
    btnEnviar.disabled = false;
    btnEnviar.classList.remove('cursor-not-allowed');
  }
}

// 9. Creemos antes de continuar una función que nos controle los errores
function mostrarError(mensaje) {
  const mensajeError = document.createElement('p');
  mensajeError.textContent = mensaje;
  mensajeError.classList.add('msjError');

  const errores = document.querySelectorAll('.msjError');
  if(errores.length === 0) {
      formulario.appendChild(mensajeError);
  }

  
}

// 12. Procedemos a enviar el formulario
function enviarEmail(e) {
  //e.preventDefault lo que hace es anular los eventos por defecto que vinieran
  e.preventDefault();
  // Mostrar el Spinner, para esto usaremos un generador de spinner: 
  const spinner = document.querySelector('#spinner');
  spinner.style.display = 'flex';

  // Usaremos una función js conocida como setTimeout para que luego de 3 segundos se oculte
  setTimeout (()=> {
    spinner.style.display = 'none';
    // Mostrar mensaje que diga que el mensaje fué enviado correctamente
    const parrafo = document.createElement('p');
    parrafo.textContent = 'El mensaje se envió correctamente';
    parrafo.classList.add('envioCorrecto');

    // Inserta el parrafo antes del spinner
    formulario.insertBefore(parrafo, spinner);

    setTimeout (()=> {
      parrafo.remove();
      resetFormulario();
    },5000);
  }, 3000);
}
// 13. Función que resetea el formulario
function resetFormulario() {
  formulario.reset();
  iniciarApp();
}
